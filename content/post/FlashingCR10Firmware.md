---
title: "Flashing CR10 Firmware"
date: 2019-02-16T08:20:38+11:00
draft: false
tags: ["3D Printing", "Guides"]
---
## Introduction

This guide assumes that you have already flashed your bootloader as I have done in my previous guide. It also assumes that you have the Sanguino board already added to your Arduino IDE

### What You'll Need

1. USB cable that came with the printer

2. CR-10

3. A capable windows computer with a free usb port

## The Process

The first thing you have to do is download new firmware that is specific to your printer. In my case I downloaded the TH3D Unified Firmware Package which works for a wide range of printers and uses a newer version of Marlin (the printer firmware) than we had before.

You can download the package [here](https://www.th3dstudio.com/downloads/TH3D_UFW_U1.R2.7_FWONLY.zip)

At the time of writing this the package version is U1.R2.7.

Extract the package onto the desktop and open the "TH3DUF_R2.ino" file. This should open to a workspace populated with different files. The main file we will be editing today is the Configuration.h file.

Open the Configuration.h file and scroll down to line number 146 where it says 
```
//#define CR10
```
uncomment the line so that it looks like the image below.


<img src="/img/Annotation 2019-02-16 082951.jpg" /> 

We've now finished modifying the firmware for the CR10. 

In my case I like to edit the boot screen to show a certain image.
If you want to do the same, open the "_Bootscreen.h" and replace the Bitmap with your own generated from one of two sites

[MarlinFW](http://www.marlinfw.org/tools/u8glib/converter.html)

[Digole](http://www.digole.com/tools/PicturetoC_Hex_converter.php)

Once thats done make sure the correct board is selcted in the IDE (Sanguino Atmega1284p for me). Connect the USB cable from the printer's control box to the computer running the IDE and select the corresponding port. 
You should now be able to Verify the Code and then Compile and Upload it to the printer.

When I first did this it spat out an error saying that U8glib.h hadn't been found. To fix this I installed and then included U8glib.h from the in-built library manager.

**Thats it, you're done!** You may notice that the UI on the LCD looks a little different, this is because of the newer version of marlin that has been used.
