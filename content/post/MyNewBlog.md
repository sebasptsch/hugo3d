---
title: "My New Blog!"
date: 2019-02-09T21:11:00+11:00
draft: false
tags: ["Announcement"]
---
Second post on a Hugo website that I thought would only be for 3D printing. Realised that it had folders so that I can discuss multiple topics without them getting in the way of one another or having to start a new website!

