---
title: "Flashing CR-10 Bootloader using Raspberry Pi"
date: 2019-02-16T06:47:46+11:00
tags: ["3D Printing", "Guides"]
draft: false
---
## Introduction

If you're like me and don't have a spare Arduino Unos hanging around but have several Raspberry Pi's and want to flash the bootloader of your CR-10, this guide is for you.

### What You'll Need

1. Raspberry Pi with soldered header (Any)

2. 6 Female-to-Female Dupont connectors

3. CR-10

4. Computer with at least one free USB port

5. USB adapter cable that came with the printer

6. A functioning Internet Connection

In this guide I will be using a windows computer, you can do this with other computers e.g. MacOS using terminal to ssh and downloading the MacOS version of the Arduino IDE

In this guide I will be using resources from [Fission3D](https://www.fission3d.com) who has his own guide for this process which I will be following to some extent.

_Make sure to unplug the main power cable from the back of the control box before starting_ **It caries mains voltage** 

## The Process

Start by making sure that nothing important is on the Raspberry Pi's SD card as it will have to have a compatible version of Debian installed. If it doesn't do so now.

Once your Pi is connected to the internet ssh into it using **Powershell** and the command
```
ssh pi@raspberrypi.local
```
the default password is _raspberry_

Enter the commands below making sure to follow the prompt to install the software
```
$ sudo apt-get update
$ sudo apt-get upgrade
## and then finally
$ sudo apt-get install avrdude
```

To verify that these commands worked correctly enter
```
$ avrdude -v
```

For the Pi to be able to interface with the CR-10 mainboard you will need to edit the default pin configuration. To do this enter
```
$ cp /etc/avrdude.conf ~/avrdude_gpio.conf #Making a new version of the configuration
```
Use the search function the find the linuxspi section of the config. This is done by pressing CTRL + W and typing in "linuxspi".

Once there you should see the default configuration which we will be changing from

```
id = "linuxspi";
desc = "Use Linux SPI device in /dev/spidev*";
type = "linuxspi";
reset = 25;
baudrate=4000000;
```

to this

```
id = "linuxspi";
desc = "Use Linux SPI device in /dev/spidev*";
type = "linuxspi";
reset = 25;
baudrate=115200; #The baudrate of the CR-10 board
```

at the bottom of the file add this config to tell the pi which of it's pins are attached to the pins of the printer's mainboard

```
# Linux GPIO configuration for avrdude.
# Change the lines below to the GPIO pins connected to the AVR.
programmer
id = "pi_1";
desc = "Use the Linux sysfs interface to bitbang GPIO lines";
type = "linuxgpio";
reset = 17;
sck = 24;
mosi = 23;
miso = 18;
```
After that's done hit CTRL + X to save and exit the file.

Wiring Time! (these images are from the Fission3D guide)
<img src="https://static.wixstatic.com/media/c10988_c05c5f97613242b297cc6876a96a6a5e~mv2_d_4360_2788_s_4_2.jpg/v1/fill/w_740,h_473,al_c,q_90,usm_0.66_1.00_0.01/c10988_c05c5f97613242b297cc6876a96a6a5e~mv2_d_4360_2788_s_4_2.webp" /> 

<img src="https://static.wixstatic.com/media/c10988_275d71e275e049ed8f4847940a8be2a8~mv2_d_4880_3008_s_4_2.jpg/v1/fill/w_740,h_456,al_c,q_90,usm_0.66_1.00_0.01/c10988_275d71e275e049ed8f4847940a8be2a8~mv2_d_4880_3008_s_4_2.webp" /> 

The mainboard on the stock CR-10 is the Atmega1284p. Make sure that your printer is also using the same board, if not then use the name of that board instead of mine.

To test the interface configuration run

```
sudo avrdude -p atmega1284p -C ~/avrdude_gpio.conf -c pi_1 –v
```

This command uses the configuration that we made earlier along with the inbuilt settings for the atmega1284p

If it responds with 

```
AVR device initiated
```

then you have successfully connected and can burn the bootloader

##Burning the Bootloader

To burn the bootloader you will need the Arduino IDE and Filezilla FTP installed and ready to use.

By default the Arduino IDE doesn't have support for Sanguino boards. To remedy that download the [Sanguino addon](https://lauszus.com/Sanguino/) and extract it to _C:\Program Files (x86)\Arduino\hardware_.

Restart the IDE and the board should show up under:
Tools > Boards > Sanguino

in this directory there should be a file called "optiboot_atmega1284p.hex"
move it to the desktop so that we can upload it later.

At this point we can open Filezilla and access the Pi using raspberrypi.local as the host, raspberry as the password and pi for the username.

move the .hex file from your desktop into the home folder of your Raspberry Pi.

Burn the bootloader making sure that the name and .hex image of the board correspond to your configuration.
```
sudo avrdude -p atmega1284p -C ~/avrdude_gpio.conf -c pi_1 -v -U flash:w:optiboot_atmega1284p.hex:i
```
At this point the printer's LCD will go blank and you will have to flash new firware.

**Done!** Your bootloader has been flashed and you can now update the printer's firmware using the blue usb cable that came with the printer.

In the next guide I will demonstrate how to flash new firmware to the printer.