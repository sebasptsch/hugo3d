---
title: "Getting My First 3D Printer"
date: 2019-02-09T20:23:50+11:00
draft: false
tags: ["3D Printing"]
---

## Why I decided to Get a 3D printer

There are several reasons why I got a 3D printer, first among them is that It gives me many new opportunities in terms of learning. I can design stuff and see what it would look like in the real world without having to mould it from clay! 
 Whilst 3D printing seems expensive and impractical for many people it can be a boon for others, enabling people to design their own products useful or not. CAD and technical experience that is involved in owning a 3D printer can serve you well later in life. 

Having not graduated high school yet a 3D printer will be useful when it comes time to start my senior art project when I can demonstrate the process of designing a useful product and all the revisions that I have made over time.

## What I plan to do with it

Now I don't have any urgent plans besides printing objects that my friends find on the internet. I've also printed the highly popular bakers’ cube and at least 5 MakerBot headphone stands (The process involves a lot of trial and error). If anyone in Sydney wants anything, I'm open to suggestions (for a price).

Meanwhile here are some cool photos that I've taken after some of my 3D prints!

<!-- ![Catapult]("/img/IMG_20190128_115919.jpg") -->
<img src="/img/IMG_20190128_115919.jpg" /> 
Headphone Stand Layering Issues

<!-- ![Layering Issues]("/img/IMG_20190206_075313.png") -->
<img src="/img/IMG_20190206_075313.png" /> 

